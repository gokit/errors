package errors_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gokit/errors"
)

func TestErrorCallGraph(t *testing.T) {
	newErr := (errors.New("failed connection: %s", "10.9.1.0")).(errors.PointingError)
	assert.Nil(t, newErr.Stack)
	assert.Equal(t, newErr.Message, "failed connection: 10.9.1.0")
	assert.Contains(t, newErr.Call.By.File, "errors_test.go")
}

func TestErrorWithStack(t *testing.T) {
	newErr := (errors.Stacked("failed connection: %s", "10.9.1.0")).(errors.PointingError)
	assert.NotNil(t, newErr.Stack)
	assert.Equal(t, newErr.Message, "failed connection: 10.9.1.0")
	assert.Contains(t, newErr.Call.By.File, "errors_test.go")
}
