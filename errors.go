package errors

import (
	"bytes"
	"fmt"
	"runtime"
	"time"
)

//***************************************
// Wrap
//***************************************

// Stacked returns an error from provided message and parameter
// list if provided. It adds necessary information related
// to point of return.
func Stacked(message string, v ...interface{}) error {
	if len(v) != 0 {
		message = fmt.Sprintf(message, v...)
	}

	var next PointingError
	next.Message = message
	next.Stack = Stack(0)
	next.Call = GetMethodGraph(3)
	return next
}

// New returns an error from provided message and parameter
// list if provided. It adds necessary information related
// to point of return.
func New(message string, v ...interface{}) error {
	if len(v) != 0 {
		message = fmt.Sprintf(message, v...)
	}

	var next PointingError
	next.Message = message
	next.Call = GetMethodGraph(3)
	return next
}

// Wrap returns a new error which wraps existing error value if
// present. It formats message accordingly with arguments from
// variadic list v.
func Wrap(err error, message string, v ...interface{}) error {
	if len(v) != 0 {
		message = fmt.Sprintf(message, v...)
	}

	var next PointingError
	next.Parent = err
	next.Message = message
	next.Call = GetMethodGraph(3)
	return next
}

// StackWrap returns a new error which wraps existing error value if
// present and also collects current stack trace into returned error.
// It formats message accordingly with arguments from
// variadic list v.
func StackWrap(err error, message string, v ...interface{}) error {
	if len(v) != 0 {
		message = fmt.Sprintf(message, v...)
	}

	var next PointingError
	next.Parent = err
	next.Stack = Stack(0)
	next.Message = message
	next.Call = GetMethodGraph(3)
	return next
}

// StackIt will attempt to add call stack into provided error if
// error is a PointingError type without a stack.
func StackIt(err error) error {
	switch po := err.(type) {
	case *PointingError:
		if po.Stack != nil {
			return po
		}

		po.Stack = Stack(0)
		return po
	case PointingError:
		if po.Stack != nil {
			return po
		}

		addr := &po
		addr.Stack = Stack(0)
		return po
	}
	return err
}

// PointingError defines a custom error type which points to
// both an originating point of return and a parent error if
// wrapped.
type PointingError struct {
	Message string
	Call    CallGraph
	Stack   *StackTrace
	Parent  error
}

// Error implements the error interface.
func (pe PointingError) Error() string {
	return pe.String()
}

// String returns formatted string.
func (pe PointingError) String() string {
	var buf bytes.Buffer
	pe.Format(&buf)
	return buf.String()
}

// Format writes details of error into provided buffer.
func (pe *PointingError) Format(buf *bytes.Buffer) {
	buf.WriteString("[!] Error ")
	buf.WriteString("Message: ")
	buf.WriteString(pe.Message)
	buf.WriteString("\n\t---- By: ")
	pe.Call.FormatBy(buf)
	buf.WriteString("\n\t---- In: ")
	pe.Call.FormatIn(buf)
	buf.WriteString("\n")

	if pe.Parent != nil {
		buf.WriteString("~")

		switch po := pe.Parent.(type) {
		case *PointingError:
			po.Format(buf)
		case PointingError:
			po.Format(buf)
		default:
			buf.WriteString(po.Error())
		}
	}
}

//**************************************************************
// StackTrace
//**************************************************************

// vars
var (
	stackSize = 1 << 6
	question  = "???"
)

// StackTrace embodies data related to retrieved
// stack collected from runtime.
type StackTrace struct {
	Stack []byte    `json:"stack"`
	Time  time.Time `json:"end_time"`
}

// Stack returns a StackTrace containing time and stack slice
// for function calls within called area based on size desired.
func Stack(size int) *StackTrace {
	if size == 0 {
		size = stackSize
	}

	trace := make([]byte, size)
	trace = trace[:runtime.Stack(trace, false)]
	return &StackTrace{
		Stack: trace,
		Time:  time.Now(),
	}
}

// String returns the giving trace timestamp for the execution time.
func (t StackTrace) String() string {
	return fmt.Sprintf("[Time=%q] %+s", t.Time, t.Stack)
}

//**************************************************************
// GetMethod
//**************************************************************

// Location defines the location which an history occured in.
type Location struct {
	Function string `json:"function"`
	Line     int    `json:"line"`
	File     string `json:"file"`
}

// CallGraph embodies a graph representing the areas where a method
// call occured.
type CallGraph struct {
	In Location
	By Location
}

// FormatBy writes giving By Location of Callgraph into byte Buffer.
func (c *CallGraph) FormatBy(by *bytes.Buffer) {
	by.WriteString(fmt.Sprintf("By[%q]: %s:%d", c.By.Function, c.By.File, c.By.Line))
}

// FormatIn writes giving In Location of Callgraph into byte Buffer.
func (c *CallGraph) FormatIn(by *bytes.Buffer) {
	by.WriteString(fmt.Sprintf("In[%q]: %s:%d", c.In.Function, c.In.File, c.In.Line))
}

// GetMethod returns the caller of the function that called it :)
func GetMethod(depth int) (string, string, int) {
	// we get the callers as uintptrs - but we just need 1
	fpcs := make([]uintptr, 1)

	// skip 3 levels to get to the caller of whoever called Caller()
	n := runtime.Callers(depth, fpcs)
	if n == 0 {
		return "Unknown()", "???", 0
	}

	funcPtr := fpcs[0]
	funcPtrArea := funcPtr - 1

	// get the info of the actual function that's in the pointer
	fun := runtime.FuncForPC(funcPtrArea)
	if fun == nil {
		return "Unknown()", "???", 0
	}

	fileName, line := fun.FileLine(funcPtrArea)

	// return its name
	return fun.Name(), fileName, line
}

// GetMethodGraph returns the caller of the function that called it :)
func GetMethodGraph(depth int) CallGraph {
	var graph CallGraph
	graph.In.File = "???"
	graph.By.File = "???"
	graph.In.Function = "Unknown()"
	graph.By.Function = "Unknown()"

	// we get the callers as uintptrs - but we just need 1
	lower := make([]uintptr, 2)

	// skip 3 levels to get to the caller of whoever called Caller()
	if n := runtime.Callers(depth, lower); n == 0 {
		return graph
	}

	lowerPtr := lower[0] - 1
	higherPtr := lower[1] - 1

	// get the info of the actual function that's in the pointer
	if lowerFun := runtime.FuncForPC(lowerPtr); lowerFun != nil {
		graph.By.File, graph.By.Line = lowerFun.FileLine(lowerPtr)
		graph.By.Function = lowerFun.Name()

	}

	if higherFun := runtime.FuncForPC(higherPtr); higherFun != nil {
		graph.In.File, graph.In.Line = higherFun.FileLine(higherPtr)
		graph.In.Function = higherFun.Name()
	}

	return graph
}

//***************************************
// internal types and functions
//***************************************
